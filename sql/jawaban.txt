cd c:\xampp\mysql\bin
cmd : mysql -uroot

Soal 1 Membuat Database :
create database myshop;

Soal 2 Membuat Table di Dalam Database :
use myshop;

table users
create Table users (
    id int(8) auto_increment,
    name varchar (255),
    email varchar (255),
    password varchar (255),
    primary key(id)
);
describe users;

table categories
create Table categories (
    id int(8) auto_increment,
    name varchar (255),
    primary key(id)
);
describe categories;

table items
create Table items (
    id int (8) auto_increment,
    name varchar (255),
    description varchar (255),
    price int (8),
    stock int (8),
    category_id int (8),
    primary key(id),
    foreign key (category_id) references categories(id)
);
describe items;

Soal 3 Memasukkan Data pada Table

table users
insert into users(name, email, password) values ("John Doe", "john@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123");
select * from users;

table categories
insert into categories(name) values ("gadget"), ("cloth"), ("men"), ("women"), ("branded");
select * from categories;

table items
insert into items(name, description, price, stock, category_id) values 
("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
select * from items;

Soal 4 Mengambil Data dari Database

a. Mengambil data users
select id, name, email from users;

b. Mengambil data items
harga di atas 1000000 (satu juta)
select * from items where price > 1000000;

name serupa atau mirip (like) dengan kata kunci "uniklo", "watch", atau "sang" (pilih salah satu saja).
select * from items where name like '%uniklo%';  

select * from items where name like '%watch%';  

select * from items where name like '%sang%';  

c. Menampilkan data items join dengan kategori

select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;


Soal 5 Mengubah Data dari Database
Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000
update items set price = 2500000 where name = "sumsang b50";
