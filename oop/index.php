<?php 
// require_once('animal.php');
require('ape.php');
require('frog.php');

// Release 0
$sheep = new Animal("shaun");
$sheep->get_name();
echo "<br>";
$sheep->get_legs();
echo "<br>";
$sheep->get_cold_blooded();
echo "<br>";
echo "<br>";


// Release 1
$kodok = new Frog("buduk");
$kodok->get_name();
echo "<br>";
$kodok->get_legs();
echo "<br>";
$kodok->get_cold_blooded();
echo "<br>";
$kodok->jump();
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
$sungokong->get_name();
echo "<br>";
$sungokong->get_legs();
echo "<br>";
$sungokong->get_cold_blooded();
echo "<br>";
$sungokong->yell();
echo "<br>";
echo "<br>";




?>